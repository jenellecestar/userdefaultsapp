//
//  ViewController.swift
//  UserDefaultsApp
//
//  Created by MacStudent on 2018-07-09.
//  Copyright © 2018 MacStudent. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        // play with userdefault here
        
        // 1. tell ios you want to use UserDefaults (localStorage from web)
        let defaults = UserDefaults.standard
        
        
        // 2. add some things to the UserDefaults
        
        // string
        defaults.set("Priteshkumar Patel", forKey:"person");
        
        // boolean
        defaults.set(true, forKey:"isInstructor")
        
        // double
        defaults.set(800.59, forKey:"hourlyRate")
        
        // array
        let coursesTaught = ["ios 101", "android 101", "swift 101", "java 101", "databases"]
        defaults.set(coursesTaught, forKey:"courses")
        
        // dictionary
        let student = ["name":"Vishavjeet Singh", "id":"C0701033", "program":"MADT", "sem":"third"]
        defaults.set(student, forKey:"student")
        
        
        
        //3. print / get stuff from userDefaults
        
        // print everything
        print(defaults.dictionaryRepresentation())
        
        print("=========")
        
        // get a specific thing from the dictory
        let x = defaults.double(forKey: "hourlyRate")
        print(x)
        
        print("Is Pritesh an instructor?")
        print(defaults.bool(forKey: "isInstructor"))
        
        print("What's his full name?")
        let name = defaults.string(forKey: "person")
        print(name!)
        
        // get an array out
        print("What courses does he teach?")
        let c = defaults.array(forKey: "courses") as! [String]
        print(c)
        
        // get a dictionary out
        print("Who is his student?")
        let d = defaults.dictionary(forKey: "student") as! Dictionary<String,String>
        print(d)
        
        
        
        
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

